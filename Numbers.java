
import java.util.Scanner;

public class Numbers {

public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	System.out.println("Enter numbers:");
	int i = input.nextInt();
	System.out.println("\nReversal of numbers:");
	reverseDisplay(i);
	input.close();

	}

public static void reverseDisplay(int value) {
	if (value != 0) {
	System.out.print(value % 10);
	value = value / 10;
	reverseDisplay(value);

	}
	}

}
